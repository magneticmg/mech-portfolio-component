<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

defined('_JEXEC') or die;
if (!class_exists('Koowa')) {
    return;
}
try {
    KObjectManager::getInstance()->getObject('com://site/portfolio.dispatcher.http')->dispatch();
} catch (Exception $exception) {
    KObjectManager::getInstance()->getObject('exception.handler')->handleException($exception);
}