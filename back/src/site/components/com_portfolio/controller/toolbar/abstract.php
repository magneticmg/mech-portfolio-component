<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
abstract class ComPortfolioControllerToolbarAbstract extends ComKoowaControllerToolbarActionbar
{

    /**
     * New toolbar command
     *
     * @param   KControllerToolbarCommand $command  A ControllerToolbarCommand object
     * @return  void
     */
    protected function _commandNew(KControllerToolbarCommand $command)
    {
        parent::_commandNew($command);

        $command->href .= '&layout=form';

    }

}