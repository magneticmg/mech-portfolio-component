<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

return [

    'identifiers' => [
//        'com://site/portfolio.database.table.portfolios' => [
//            'behaviors' => [
//                'creatable',
//                //'parameterizable'
//            ],
//            //'filters' => [ 'meta' => ['json']]
//        ]
    ],
    'aliases' => [
        'investments' => 'com://site/portfolio.model.investments',
        'investment' => 'com://site/portfolio.controller.investment',
        'portfolios' => 'com://site/portfolio.model.portfolios',
        'portfolio' => 'com://site/portfolio.controller.portfolio',

    ]
];