<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2015 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 *
 *
 */

/**
 * Class ComPortfolioDatabaseTablePortfolios
 * @mixin KCommandMixin
 */
class ComPortfolioDatabaseTablePortfolios extends KDatabaseTableAbstract
{

    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array('behaviors' => array('creatable')));

        parent::_initialize($config);
    }
}