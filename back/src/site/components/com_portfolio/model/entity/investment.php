<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2015 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioModelEntityInvestment extends KModelEntityRow
{

    protected $_company;
    /**
     * Get the current value of the investment based on the latest data.
     * @todo: cache this data locally for x minutes to speed it up.
     */
    public function getCurrentValue()
    {

        $options = new JRegistry(array('headers' => [
            'x-rapidapi-key' => '2b442c6d97mshf94ffaf078e4994p1f4c61jsnbb1e58a712e7',
            'x-rapidapi-host' => 'apidojo-yahoo-finance-v1.p.rapidapi.com'
        ]));
        $client = JHttpFactory::getHttp($options, 'curl');

        /**
         * @var $response JHttpResponse
         */
        $response = $client->get('https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-profile?symbol=' . $this->getCompany()->symbol .'&region=US');


        $jresponse = json_decode($response->body);
        
        $price = $jresponse->price->regularMarketOpen->raw;

        return $price;

        return 1000;
    }

    public function getCompany()
    {
        if(!$this->_company)
        {
            $this->_company = $this->getObject('com://site/portfolio.model.company')->id($this->company_id)->fetch();
        }

        return $this->_company;
    }
}