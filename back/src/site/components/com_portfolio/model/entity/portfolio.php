<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioModelEntityPortfolio extends KModelEntityRow
{

    protected $investments;

    /**
     * @return KModelEntityRowset
     */
    public function getInvestments()
    {
        if(!$this->investments)
        {
            $this->investments = $this->getObject('investments')->fetch();
        }

        return $this->investments;
    }

}