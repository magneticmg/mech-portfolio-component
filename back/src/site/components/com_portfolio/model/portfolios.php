<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioModelPortfolios extends KModelDatabase
{

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->getState()
            //->insert('user_id', 'int', $this->getObject('user')->getId())
            ->insert('created_by', 'int', $this->getObject('user')->getId());


    }

    protected function _buildQueryWhere(KDatabaseQueryInterface $query)
    {
        $state = $this->getState();

        if($created_by = $state->get('created_by'))
        {
            $query->where('tbl.created_by = :created_by')->bind(array('created_by' => $created_by));
        }


//        $user = $this->getObject('user')->getId();
//
//        if(is_numeric($user))
//        {
//            $query->where('tbl.user_id = :user_id')->bind(array('user_id' => $user));
//        }

    }
}