<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioModelInvestments extends KModelDatabase
{

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->getState()
             ->insert('portfolio_id', 'int');
    }

    protected function _buildQueryWhere(KDatabaseQueryInterface $query)
    {
        $state = $this->getState();


        if($portfolio = $state->get('portfolio_id'))
        {
            $query->where('tbl.portfolio_id = :portfolio_id')->bind(array('portfolio_id' => $portfolio));
        }
    }
}