<?php

class ComPortfolioTemplateHelperActions extends KTemplateHelperBehavior
{
    public function portfolio($config = array())
    {
        $config = new KObjectConfig($config);

        $entity = $config->entity;


        $config->append(array(
            'actions' => array(
                'configure' => array(
                    'title' => 'Edit',
                    'icon' => 'k-icon-pencil',
                    'url'  => 'index.php?option=com_portfolio&view=portfolio&layout=form&id=' . $entity->id,
                ),
                'start' => array(
                    'title' => 'Add Investment',
                    'icon' => 'k-icon-arrow-circle-right',
                    'url'  => 'index.php?option=com_portfolio&view=investment&layout=form&id=&portfolio_id=' . $entity->id,
                ),
                'summary' => array(
                    'title' => 'Summary',
                    'icon' => 'k-icon-pie-chart',
                    'url'  => 'index.php?option=com_portfolio&view=portfolio&layout=indices&id=' . $entity->id
                ),
            )
        ));

        return $this->_renderHtml($config->actions);
    }

    public function investment($config = array())
    {
        $config = new KObjectConfig($config);
        $entity = $config->entity;

        $config->append(array(
            'actions' => array(
                'configure' => array(
                    'title' => 'Edit',
                    'icon' => 'k-icon-pencil',
                    'url'  => 'index.php?option=com_portfolio&view=investment&layout=form&id=' . $entity->id,
                ),
                'summary' => array(
                    'title' => 'Compare to Indices',
                    'icon' => 'k-icon-pie-chart',
                    'url'  => 'index.php?option=com_portfolio&view=investments&layout=indices&id='. $entity->id,
                ),
            )
        ));

        return $this->_renderHtml($config->actions);
    }

    protected function _renderHtml($actions)
    {
        $html = '';

        foreach($actions as $action)
        {
            $html .= '<a href="' . $action['url'] . '" title="' . $action['title'] . '">
                <span class="' . $action['icon'] . '" aria-hidden="true"></span>
                <span class="k-visually-hidden">' . $action['title'] . '</span>
            </a>';
        }

        return $html;
    }
}
