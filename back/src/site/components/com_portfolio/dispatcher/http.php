<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioDispatcherHttp extends ComKoowaDispatcherHttp
{

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->addCommandCallback('before.dispatch', 'canDispatchAndRedirect');
    }

    public function canDispatchAndRedirect($context)
    {
        if(!$this->getUser()->isAuthentic())
        {
            $context->response->setStatus('302', 'You must be logged in');

            $context->param = 'index.php?option=com_users&view=login&return=' . base64_encode($context->request->getUrl());

            return $this->_actionRedirect($context);

        }
    }
}