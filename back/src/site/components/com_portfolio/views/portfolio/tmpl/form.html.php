<?php
/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

/**
 * Portfolio form layout
 */

?>
<?= helper('ui.load'); ?>

<? JHtml::stylesheet('com_portfolio/portfolio.css', array('relative' => true)); ?>

<?= helper('behavior.vue', ['entity' => $portfolio, 'debug' => true]); ?>
<script>document.documentElement.className += " k-frontend-ui";</script>

<!-- TOP -->
<div class="k-top-container">
    <div class="k-top-container__logo">
        <?= $portfolio->title ? $portfolio->title :  translate('COM_PORTFOLIO_MY_PORTFOLIO_CONFIGURATION_TITLE') ?> :<?= translate('COM_PORTFOLIO_MY_PORTFOLIO_CONFIGURATION') ?>
    </div>
</div>
<!-- Wrapper -->

<div class="k-wrapper k-js-wrapper">

    <!-- Overview -->
    <div class="k-content-wrapper">

        <ktml:module position="right"><?= import('form_sidebar_navigation.html') ?></ktml:module>

        <!-- The content -->
        <div class="k-content k-js-content">

            <!-- Toolbar -->
            <ktml:toolbar type="actionbar">

            <!-- Component wrapper -->
            <div class="k-component-wrapper">

                <!-- Component -->
                <form class="k-component k-js-component k-js-form-controller" action="" method="post">

                    <!-- Container -->
                    <div class="k-container">

                        <!-- Main information -->


                        <div class="k-container__full">

                            <div class="k-form-group">
                                <div class="k-input-group k-input-group--large">
<!--                                        <label for="input-form-group-1">--><?//= translate('Portfolio Title') ?>
                                    <input required
                                            class="k-form-control"
                                            id="portfolio_form_title"
                                            type="text"
                                            name="name"
                                            maxlength="255"
                                            placeholder="<?= translate('Title') ?>"
                                            value="<?= escape($portfolio->name); ?>" />
                                </div>
                            </div>

<!--                            <div class="k-form-group">-->
<!--                                <label for="input-form-group-2">--><?//= translate('COM_PORTFOLIO_PORTFOLIO_TYPE') ?><!--</label>-->
<!---->
<!--                                --><?//= helper('listbox.portfolioType', array(
//                                    'selected' => $portfolio->type,
//                                    'attribs' => array(
//                                        'id' => 'proj-type'
//                                ))); ?>
<!--                            </div>-->

                        </div><!-- .k-container__main -->

                    </div><!-- .k-container -->

                </form><!-- .k-component -->

            </div><!-- .k-component-wrapper -->

            <!-- Toolbar -->
            <ktml:toolbar type="actionbar">

        </div><!-- .k-content -->

    </div><!-- .k-content-wrapper -->

</div><!-- .k-wrapper -->



<!--<oscript data-inline src="media://com_portfolio/js/portfolio.js"></oscript>-->
