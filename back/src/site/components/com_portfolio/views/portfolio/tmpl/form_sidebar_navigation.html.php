<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */


if(isset($portfolio)){
    $investments = $portfolio->getInvestments();
} else {
    $portfolio = new stdClass();
    $portfolio->id = false;
}

$layout = isset($layout) ? $layout : 'form';

?>
<div class="k-sidebar-left k-js-sidebar-left">

    <!-- Navigation -->
    <div class="k-sidebar-item k-sidebar-item--flex">
        <ul class="k-list">
            <? if (isset($investments)) : ?>
            <li class="k-is-active">
                <a href="<?= route('view=investments') ?>">
                    <?= translate('Current Investments'); ?>
                </a>
            </li>
            <? endif; ?>
            <li>
                <a href="<?= route('view=investment&layout=form&portfolio_id=' . $portfolio->id) ?>">
                    <?= translate('Add Investment'); ?>
                </a>
            </li>
        </ul>
    </div>

</div>