<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
/**
 * @var $portfolio ComPortfolioModelEntityPortfolio
 */
?>
<?= helper('ui.load'); ?>
<div class="k-top-container">
    <div class="k-top-container__logo">
    <h1><?= translate('COM_PORTFOLIO_PORTFOLIO') ?>: <?= $portfolio->name ?></h1>
    </div>
</div>

<div class="k-wrapper k-js-wrapper">

    <!-- Overview -->
    <div class="k-content-wrapper">

        <ktml:module position="position-7"><?= import('form_sidebar_navigation.html', array('layout' => 'portfolios', 'portfolio' => $portfolio)) ?></ktml:module>

        <!-- The content -->
        <div class="k-content k-js-content">

            <!-- Component wrapper -->
            <div class="k-component">
<h2><?= translate('COM_PORTFOLIO_MY_INVESTMENTS') ?></h2>

<?= object('com://site/portfolio.controller.investment')
    ->portfolio_id($portfolio->id)
    ->view('investments')
    ->layout('default_table')
    ->render() ?>
            </div>
        </div>
    </div>
</div>