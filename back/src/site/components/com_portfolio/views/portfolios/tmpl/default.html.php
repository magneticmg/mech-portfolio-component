<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

if (empty($params)) {
    $params = new JRegistry;
}

?>
<?= helper('ui.load', ['k_ui_container'=>true]); ?>

<div class="k-top-container">
    <div class="k-top-container__logo">
        <h2><?= translate('My Portfolios') ?></h2>
    </div>
</div>

<!-- Wrapper -->
<div class="k-wrapper k-js-wrapper">

    <!-- Overview -->
    <div class="k-content-wrapper">

        <ktml:module position="position-7"><?= import('com://site/portfolio.portfolio.form_sidebar_navigation.html', array('layout' => 'portfolios')) ?></ktml:module>

        <!-- The content -->
        <div class="k-content k-js-content">

            <!-- Toolbar -->
            <ktml:toolbar type="actionbar">

                <!-- Component wrapper -->
                <div class="k-component">

                    <!-- Component -->
                    <form class="k-flex-wrapper k-js-grid-controller" action="" method="get">

                        <div class="k-table-container">
                            <div class="k-table">
                        <? if (count($portfolios)) : ?>
                            <table class="k-js-fixed-table-header k-js-responsive-table">

                                <thead>

                                    <tr>

                                        <th width="1%" class="k-table-data--form">
                                            <?= helper('grid.checkall')?>
                                        </th>

                                        <th class="k-align-left"><?= translate('Name'); ?></th>

                                        <th class="k-align-left"><?= translate('Actions'); ?></th>

                                    </tr>

                                </thead>

                                <tbody>
                                    <? foreach ($portfolios as $portfolio): ?>
                                    <tr>
                                        <td class="k-table-data--form">
                                            <?= helper('grid.checkbox', array(
                                                'entity' => $portfolio
                                            ))?>
                                        </td>

                                        <td class="portfolio_portfolio__name">

                                                <a href="<?= route('view=portfolio&id=' . $portfolio->id) ?>" title="Configure">
                                                    <?= $portfolio->name?>
                                                </a>


                                            <div class="portfolio_portfolio__description k-ellipsis">
                                                <?= $portfolio->description?>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="portfolio_portfolio__actions" style="margin: 5px 5px 10px 20px">
                                                <?= helper('actions.portfolio', array('entity' => $portfolio)); ?>
                                            </div>
                                        </td>

                                    </tr>
                                    <? endforeach ?>
                                </tbody>

                            </table>

                            <div class="k-table-pagination">
                                <?= helper('paginator.pagination') ?>
                            </div><!-- .k-table-pagination -->
                        <? else : ?>
                            <p style="text-align:center;">
                                <em><?= translate('No Portfolio Has Been Set'); ?></em>
                            </p>
                        <? endif; ?>
                        </div>
                        </div>

                    </form><!-- .k-component -->

                </div><!-- .k-component-wrapper -->

        </div><!-- .k-content -->

    </div><!-- .k-content-wrapper -->

</div><!-- .k-wrapper -->
