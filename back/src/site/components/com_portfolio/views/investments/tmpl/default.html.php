<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

?>
<h1><?= translate('COM_PORTFOLIO_MY_INVESTMENTS') ?></h1>
<?= import('default_table.html') ?>