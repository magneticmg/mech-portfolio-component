<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
?>
<h2>Indices/Chart</h2>
<div class="">
    Here is where we show how investments might stack-up against one or several market indices.
</div>