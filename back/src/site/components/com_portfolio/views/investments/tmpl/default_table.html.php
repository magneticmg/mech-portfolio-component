<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */


?>
<style>
    table.th { align: left}
</style>
<div class="k-table-container">

    <div class="k-table">

        <? if (count($investments)): ?>
            <table class="k-js-fixed-table-header k-js-responsive-table" width="100%">

                <thead>

                <tr>


                        <th width="1%" class="k-table-data--form">
                            <?= helper('grid.checkall')?>
                        </th>

                        <th><?= translate('COM_PORTFOLIO_TITLE'); ?></th>
                        <th><?= translate('Quick Summary'); ?></th>
                        <th>Date</th>
                        <th>Actions</th>
                </tr>

                </thead>

                <tbody>
                <? foreach ($investments as $investment):?>
                    <tr>

                            <td class="k-table-data--form">
                                <?= helper('grid.checkbox', array(
                                    'entity' => $investment
                                ))?>
                            </td>
                            <td class="k-left">
                            <?= $investment->name ?>
                            </td>
                        <td >
                            <?= import('com://site/portfolio.investment.default_list_summary.html', array(
                                'investment'    => $investment
                            )) ?>
                        </td>
                            <td><?= date('Y-m-d', strtotime($investment->created_on)); ?></td>

                            <td>
                                <div class="portfolio_investment__actions" style="margin: 5px 5px 10px 20px">
                                    <?= helper('actions.investment', array('entity' => $investment)); ?>
                                </div>
                            </td>



                    </tr>
                <? endforeach ?>
                </tbody>

            </table>

            <div class="k-table-pagination">
                <?= helper('paginator.pagination') ?>
            </div><!-- .k-table-pagination -->
        <? else : ?>
            <p style="text-align:center;">
                <em><?= translate('COM_PORTFOLIO_NO_ITEMS'); ?></em>
            </p>
            <p style="text-align:center;">
            <a href="<?= route('view=investment&layout=form&portfolio_id=' . JFactory::getApplication()->input->get('id')) ?>"><?= translate('COM_PORTFOLIO_START_NEW_INVESTMENT') ?></a>
            </p>
        <? endif; ?>
    </div>

</div>