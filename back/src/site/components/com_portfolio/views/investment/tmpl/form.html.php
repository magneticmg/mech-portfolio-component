<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */

/**
 * @var $investment KModelEntityRow
 */
?>

<?= helper('ui.load'); ?>

<? JHtml::stylesheet('com_portfolio/portfolio.css', array('relative' => true)); ?>

<?= helper('behavior.vue', ['entity' => $investment, 'debug' => true]); ?>
<script>document.documentElement.className += " k-frontend-ui";</script>

<!-- TOP -->
<div class="k-top-container">
    <div class="k-top-container__logo">
        <?= translate('COM_PORTFOLIO_INVESTMENT_LAYOUT_TITLE') ?> :<?= $investment->id ? $investment->getCompany()->name : translate('COM_PORTFOLIO_START_NEW_INVESTMENT')?>
    </div>
</div>
<!-- Wrapper -->

<div class="k-wrapper k-js-wrapper">

    <!-- Overview -->
    <div class="k-content-wrapper">



        <!-- The content -->
        <div class="k-content k-js-content">

            <!-- Toolbar -->
            <ktml:toolbar type="actionbar">

                <!-- Component wrapper -->
                <div class="k-component-wrapper">

                    <!-- Component -->
                    <form class="k-component k-js-component k-js-form-controller" action="" method="post">

                        <!-- Container -->
                        <div class="k-container">

                            <!-- Main information -->

                            <div class="k-container__full">

                                <div class="k-form-group">
                                    <label for="input-1">Name</label>
                                    <input type="text" id="input-1" name="name" placeholder="Give your investment a name" value="<?= $investment->name ?>"/>

                                </div>
                                <div class="k-form-group">
                                    <label for="input-2">Company</label>
                                    <?= helper('listbox.companies', array(
                                        'model' => 'companies',
                                        'name' => 'company_id',
                                        'value'    => 'id',
                                        'label'    => 'name',
                                        'filter'   => array('sort' => 'name')
                                    )); ?>

                                </div>
                                <div class="k-form-group">
                                    <label for="exchange">Exchange</label>
                                    <?= helper('listbox.exchange', array(
                                        'model' => 'exchanges',
                                        'name' => 'exchange_id',
                                        'value'    => 'id',
                                        'label'    => 'name',
                                        'filter'   => array('sort' => 'name')
                                    )); ?>

                                </div>
                                <div class="k-form-group">
                                    <label>Shares Purchased</label><input type="text" name="shares" id="" value="<?= $investment->shares ?>"/>
                                </div>
                                <div class="k-form-group">
                                    <label>Share Cost</label><input type="text" name="share_cost" id="" value="<?= $investment->share_cost ?>"/>
                                </div>
                                <div class="k-form-group">
                                    <label>Total Cost</label><input type="text" name="total_cost" id="" value="<?= $investment->total_cost ?>"/>
                                </div>

                                <input name="portfolio_id" type="hidden" value="<?= $investment->portfolio_id ? $investment->portfolio_id  : JFactory::getApplication()->input->get('portfolio_id')?>">
                            </div><!-- .k-container__main -->

                        </div><!-- .k-container -->

                    </form><!-- .k-component -->

                </div><!-- .k-component-wrapper -->

                <!-- Toolbar -->
                <ktml:toolbar type="actionbar">

        </div><!-- .k-content -->

    </div><!-- .k-content-wrapper -->

</div><!-- .k-wrapper -->
