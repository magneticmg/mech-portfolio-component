<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
/**
 * @var $investment
 */
?>
<div>
    Current Value: $<?= number_format($investment->getCurrentValue(), 2) ?>
</div>
