<?php

/**
 * @package
 * @SubPackage
 * @copyright    Copyright (C) 2021 Magnetic Merchandising Inc. All rights reserved.
 * @license      No License
 * @link        http://magneticmerchandising.com
 */
class ComPortfolioControllerPermissionPortfolio extends KControllerPermissionAbstract
{

    protected function _hasAccess()
    {

        $result = $this->getMixer()->getModel()->fetch();

        return $result->getProperty('user_id') == $this->getObject('user')->id;
    }

    public function canEdit()
    {
         return parent::canEdit() && $this->_hasAccess();
    }

    public function canDelete()
    {
        return $this->canEdit();
    }

}